/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.actionbar_clustermenu;

import android.app.Activity;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu.OnMenuItemClickListener;
import com.example.actionbar_clustermenu.CustomMenu.DropDownMenu;

public class ActionModeHandler implements ActionMode.Callback {
    private Activity  mActivity;
    private DropDownMenu mSelectionMenu;
    private boolean selectAllState = false;

    public ActionModeHandler(Activity activity) {
    	mActivity = activity;
    }

    public ActionMode startActionMode() {
        final ActionMode actionMode = mActivity.startActionMode(this);
        View customView = LayoutInflater.from(mActivity).inflate(
                R.layout.action_mode, null);
        actionMode.setCustomView(customView);
        CustomMenu customMenu = new CustomMenu(mActivity);
        mSelectionMenu = customMenu.addDropDownMenu(
                (Button) customView.findViewById(R.id.selection_menu),
                R.menu.selection);
        updateSelectionMenu();
        customMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                return onActionItemClicked(actionMode, item);
            }
        });
        return actionMode;
    }

    public void setTitle(String title) {
        mSelectionMenu.setTitle(title);
    }


    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        if (item.getItemId() == R.id.action_select_all) {
            updateSelectionMenu();
        }
        return true;
    }

    private void updateSelectionMenu() {
        // update title
        setTitle("oh yeah");
        if(selectAllState){
        	selectAllState = false;
        }else {
        	selectAllState = true;
        }
        // For clients who call SelectionManager.selectAll() directly, we need to ensure the
        // menu status is consistent with selection manager.
        MenuItem item = mSelectionMenu.findItem(R.id.action_select_all);
        if (item != null) {
            if (selectAllState) {
                item.setChecked(true);
                item.setTitle(R.string.deselect_all);
            } else {
                item.setChecked(false);
                item.setTitle(R.string.select_all);
            }
        }
    }

    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        return true;
    }

    public void onDestroyActionMode(ActionMode mode) {
        //leave current UI to before activity 
    }

    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return true;
    }


}
